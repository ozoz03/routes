package org.oz.controller;

import org.oz.controller.response.RouteResponse;
import org.oz.repository.RepositoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoutesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoutesController.class);

    @Autowired
    private RepositoryManager repositoryManager;

    @RequestMapping(value = "/api/direct", method = RequestMethod.GET)
    RouteResponse getRoutes(@RequestParam("dep_sid") int depSid, @RequestParam("arr_sid") int arrSid) {
        return new RouteResponse(depSid, arrSid, repositoryManager.isDirectRoute(depSid, arrSid));
    }

}
