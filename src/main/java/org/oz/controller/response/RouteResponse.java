package org.oz.controller.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RouteResponse {
    @JsonProperty("dep_sid")
    int depSid;

    @JsonProperty("arr_sid")
    int arrSid;

    @JsonProperty("direct_bus_route")
    boolean directBusRoute;

    public RouteResponse(int depSid, int arrSid, boolean directBusRoute) {
        this.depSid = depSid;
        this.arrSid = arrSid;
        this.directBusRoute = directBusRoute;
    }

    public int getDepSid() {
        return depSid;
    }

    public int getArrSid() {
        return arrSid;
    }

    public boolean isDirectBusRoute() {
        return directBusRoute;
    }
}
