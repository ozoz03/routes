package org.oz.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.oz.repository.RepositoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Importer for data file into repository.
 * Expects CSV data file with sapce as delimiter.
 * First line of the file is number of lines.
 */
@Component
public class DataFileRepositoryImporter {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataFileRepositoryImporter.class);
    private static final String DELIMITER = " ";

    @Autowired
    private RepositoryManager repositoryManager;

    void importFile(File dataFile) {
        LOGGER.info("Loading data from {}", dataFile.getAbsolutePath());

        int count = importFileIntoRepository(dataFile);
        LOGGER.info("Was loaded {} records from {}", count, dataFile.getAbsolutePath());
    }

    private int importFileIntoRepository(File dataFile) {
        int count = 0;
        try (BufferedReader fileReader = new BufferedReader(new FileReader(dataFile))) {
            String line;
            // Read routes number
            Integer routeNumber = Integer.parseInt(fileReader.readLine());

            while ((line = fileReader.readLine()) != null && count < routeNumber) {
                count++;
                String[] tokens = line.split(DELIMITER);
                repositoryManager.addNewRouteToRepository(tokens);
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File {} was not found", dataFile.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.error("File {} read exception : {}", dataFile.getAbsolutePath(), e.getMessage());
        }
        return count;
    }


}
