package org.oz.importer;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Runs on the application start up.
 * Tries to run data file importer.
 * Data file name will be taken as first arg of the application.
 */
@Component
public class DataFileImporterRunner implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataFileImporterRunner.class);

    @Autowired
    private DataFileRepositoryImporter importer;

    @Override
    public void run(String... args) throws Exception {
        if (args.length < 1 || StringUtils.isEmpty(args[0])) {
            LOGGER.error("Data file name argument is empty. Nothing to load");
            return;
        }
        String dataFilePathArg = args[0];
        File dataFile = new File(dataFilePathArg);
        if (dataFile.exists() && !dataFile.isDirectory()) {
            importer.importFile(dataFile);
        } else {
            LOGGER.error("Data file does not exist : {}", dataFilePathArg);
        }
    }
}
