package org.oz.repository;

import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * A repository service.
 */
@Component
public class RepositoryManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryManager.class);

    private DataRepository repository = new DataRepository();

    /**
     * Adds Route to the Repository
     * @param tokens is a String array, the first member is String presentation of Route number, others are Station numbers
     */
    public void addNewRouteToRepository(String[] tokens) {
        Integer routeNumber = Integer.parseInt(tokens[0]);
        for (int i = 1; i < tokens.length; i++) {
            Integer stationNumber = Integer.parseInt(tokens[i]);
            Set<Integer> routes = repository.getRotesForStation(stationNumber);
            if (routes == null) {
                HashSet<Integer> newRouteSet = new HashSet<>();
                newRouteSet.add(routeNumber);
                repository.addRoutesForStation(stationNumber, newRouteSet);
                LOGGER.info("Added new Station {}", stationNumber, routeNumber);
            } else {
                routes.add(routeNumber);
                LOGGER.info("Added Route {} to the Station {}", routeNumber, stationNumber);
            }
        }
        LOGGER.info("Added Route : {}", routeNumber);
    }


    /**
     * Answers the question are the Stations connected with any Road.
     * The same Station is connected to itself.
     * @param depSid is the departure Station Id
     * @param arrSid is the arrival Station Id
     * @return
     */
    public boolean isDirectRoute(int depSid, int arrSid) {
        Set<Integer> station1Routes = repository.getRotesForStation(depSid);
        Set<Integer> station2Routes = repository.getRotesForStation(arrSid);
        if (station1Routes == null || station2Routes == null) {
            return false;
        }
        Set<Integer> intersection = Sets.intersection(station1Routes, station2Routes);

        return !intersection.isEmpty();
    }
}
