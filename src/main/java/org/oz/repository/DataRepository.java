package org.oz.repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Repository of Roads.
 * Contains a map Station to Route set where the Station is used.
 */
public class DataRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataRepository.class);

    private Map<Integer, Set<Integer>> stationToRoutesMap = new HashMap<>();


    public void addRoutesForStation(Integer station, HashSet<Integer> routeSet) {
        stationToRoutesMap.put(station, routeSet);
    }

    public Set<Integer> getRotesForStation(Integer station) {
        return stationToRoutesMap.get(station);
    }
}
