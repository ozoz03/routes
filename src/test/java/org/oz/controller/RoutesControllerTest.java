package org.oz.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.oz.controller.response.RouteResponse;
import org.oz.repository.RepositoryManager;

@RunWith(MockitoJUnitRunner.class)
public class RoutesControllerTest {
    private static final int DEP_SID = 3;
    private static final int ARR_SID = 6;

    @Mock
    RepositoryManager repositoryManager;

    @InjectMocks
    RoutesController routesController;


    @Test
    public void getRoutes() throws Exception {
        RouteResponse controllerResponse = routesController.getRoutes(DEP_SID, ARR_SID);

        // validate response
        assertEquals(DEP_SID,controllerResponse.getDepSid());
        assertEquals(ARR_SID,controllerResponse.getArrSid());

        final JsonNode schemaNode = JsonLoader.fromPath(this.getClass().getClassLoader().getResource("get-schema.json").getPath());
        ObjectMapper mapper = new ObjectMapper();
        final JsonNode response = JsonLoader.fromString(mapper.writeValueAsString(controllerResponse));
        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        final JsonSchema schema = factory.getJsonSchema(schemaNode);

        // validate schema
        assertTrue(schema.validate(response).isSuccess());
    }

}