package org.oz.importer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.PrintWriter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.oz.repository.RepositoryManager;

@RunWith(MockitoJUnitRunner.class)
public class DataFileRepositoryImporterTest {

    private static final String DELIMITER = " ";
    @InjectMocks
    DataFileRepositoryImporter importer;

    @Mock
    RepositoryManager repositoryManager;
    private File testFile;
    private String string1 = "1 2 3";
    private String string2 = "3 4 3";
    private String string3 = "567 24 3444";


    @Before
    public void setUp() throws Exception {
        testFile = File.createTempFile("myDataFile", ".tmp");
        try (PrintWriter out = new PrintWriter(testFile)) {
            out.println("3");
            out.println(string1);
            out.println(string2);
            out.println(string3);
        }
    }

    @Test
    public void importFile() throws Exception {
        importer.importFile(testFile);

        verify(repositoryManager, times(1)).addNewRouteToRepository(string1.split(DELIMITER));
        verify(repositoryManager, times(1)).addNewRouteToRepository(string2.split(DELIMITER));
        verify(repositoryManager, times(1)).addNewRouteToRepository(string3.split(DELIMITER));
    }

}