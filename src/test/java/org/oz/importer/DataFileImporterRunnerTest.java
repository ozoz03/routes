package org.oz.importer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DataFileImporterRunnerTest {

    @Mock
    DataFileRepositoryImporter importer;

    @InjectMocks
    DataFileImporterRunner runner;

    @Test
    public void testImporterWasRunDataFileExists() throws Exception {
        File testFile = File.createTempFile("myDataFile", ".tmp");
        runner.run(testFile.getPath());
        verify(importer, times(1)).importFile(testFile);
    }

    @Test
    public void testImporterWasNotRunNoArgs() throws Exception {
        runner.run();
        verify(importer, times(0)).importFile(any(File.class));
    }

    @Test
    public void testImporterWasNotRunFileDoesNotExist() throws Exception {
        runner.run("myFile");
        verify(importer, times(0)).importFile(any(File.class));
    }
}