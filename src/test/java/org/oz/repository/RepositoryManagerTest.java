package org.oz.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RepositoryManagerTest {

    @InjectMocks
    RepositoryManager repositoryManager;

    @Mock
    DataRepository dataRepository;
    private String[] strings = {"7", "1", "3", "5"};
    private Set<Integer> routes = new HashSet<>(2);

    @Before
    public void setUp() {
        routes.add(2);
        routes.add(5);
        when(dataRepository.getRotesForStation(3)).thenReturn(routes);
        when(dataRepository.getRotesForStation(1)).thenReturn(null);
        when(dataRepository.getRotesForStation(5)).thenReturn(null);
    }

    @Test
    public void addNewRouteToRepository() throws Exception {
        repositoryManager.addNewRouteToRepository(strings);

        Set<Integer> expectedRouteSet = new HashSet(3);
        expectedRouteSet.add(2);
        expectedRouteSet.add(5);
        expectedRouteSet.add(7);
        assertTrue(routes.containsAll(expectedRouteSet));

        HashSet<Integer> newRoute = new HashSet<>(1);
        newRoute.add(7);
        verify(dataRepository, times(1)).addRoutesForStation(1, newRoute);
        verify(dataRepository, times(1)).addRoutesForStation(5, newRoute);
    }

    @Test
    public void isDirectRouteTrue() throws Exception {
        when(dataRepository.getRotesForStation(155)).thenReturn(routes);

        assertTrue(repositoryManager.isDirectRoute(3, 155));
    }

    @Test
    public void isDirectRouteTheSame() throws Exception {
        assertTrue(repositoryManager.isDirectRoute(3, 3));
    }

    @Test
    public void isDirectRouteFalse() throws Exception {
        assertFalse(repositoryManager.isDirectRoute(1, 5));
    }

}